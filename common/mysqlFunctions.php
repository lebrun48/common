<?php

//-------------------------------------------------------------------------------------------------
function jmysql_query($sql, $func = -1, $db = null)
{
  global $serverConfig, $seeRequest, $currentDBLink;
  if ($func == -1) {
    $func = $seeRequest & 7;
  } elseif ($func != 0) {
    $func |= $seeRequest & 7;
  }
  if ($func & 1)
    debugLog($sql, "sql", $seeRequest & 8);
  if ($func & 2 && strcasecmp(substr($sql, 0, 7), "select ") && strcasecmp(substr($sql, 0, 5), "show ")) {
    debugLog("<span style=color:red;font-weight:bold>Not executed</span>", null, $seeRequest & 8);
    return true;
  }
  if (!$db)
    $db = $currentDBLink;
  if ($serverConfig->isDevPhase && !($func & 4))
    $res = $db->query($sql);
  else
    $res = @$db->query($sql);
  if (!$res) {
    if (strpos($sql, "'A=0") !== false)
      stop(__FILE__, __LINE__);
    $l = $db->error;
    if ($serverConfig->isDevPhase)
      debugLog($l . nl . "sql=$sql" . nl, $seeRequest & 8);
    else if (SendError()) {
      mail("lebrun48@gmail.com", "!!Error mysql", $l . nl . "sql=$sql\n" . getErrorDump());
      //echo "lebrun48@gmail.com", "!!Error mysql", $l . nl . "sql=$sql\n" . getErrorDump();
    }
  }
  return $res;
}

function jmysql_escape_string($str)
{
  global $currentDBLink;
  return $currentDBLink->escape_string($str);
}

function jmysql_real_escape_string($str)
{
  global $currentDBLink;
  return $currentDBLink->real_escape_string($str);
}

function jmysql_result($res, $row, $attr = 0)
{
  return $res->data_seek($row) ? (is_string($attr) ? $res->fetch_assoc()[$attr] : $res->fetch_row()[$attr]) : null;
}

function jmysql_num_rows($res)
{
  return $res->num_rows;
}

function jmysql_fetch_row($res)
{
  return $res->fetch_row();
}

function jmysql_fetch_assoc($res)
{
  return $res->fetch_assoc();
}

function jmysql_error()
{
  global $currentDBLink;
  return $currentDBLink->error;
}

function jmysql_affected_rows()
{
  global $currentDBLink;
  return $currentDBLink->affected_rows;
}

function jmysql_data_seek($res, $row)
{
  return $res->data_seek($row);
}

function jmysql_errno()
{
  global $currentDBLink;
  return $currentDBLink->errno;
}
