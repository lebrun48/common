<?php

trait DBUtil
{
  private function getUpdatedCols()
  {
    global $request;
    $where = getPrimary();
    foreach ($request as $k => $v) {
      if (substr($k, 0, 4) != "val_")
        continue;
      $k = substr($k, 4);
      if (strlen($v) == 0) {
        if (!$bList || isset($request["_replace_$k"])) {
          $sql .= "$k=null, ";
        }
      } else if ($v[0] == '=') {
        $sql .= "$k$v, ";
      } else {
        $quote = is_numeric($v) ? '' : "'";
        if ($bList && $quote && !isset($request["_replace_$k"]))
          $sql .= "$k=concat('" . addslashes($v) . " ',ifnull($k,'')), ";
        else
          $sql .= " $k=$quote" . addslashes($v) . $quote.", ";
      }
    }
    return substr($sql, 0, -2) . $where;
  }

  private function updateRow()
  {
    global $systemError;
    if (!$sql = $this->getUpdatedCols()) {
      return;
    }
    $sql = "update " . $this->dbTableName . " set " . $sql . ($bList ? '' : " limit 1");
    if (!jmysql_query($sql, 4)) {
      stop(__FILE__, __LINE__, "(" . jmysql_errno() . ") " . jmysql_error() . ": $sql");
      return $systemError;
    }
  }

  private function deleteRow()
  {
    if (!$sql = $this->getUpdatedCols()) {
      return;
    }
    jmysql_query("delete from " . $this->dbTableName . getPrimary()." limit 1");
  }

  private function insertRow()
  {
    if (!$sql = $this->getUpdatedCols()) {
      return;
    }
    jmysql_query("insert into " . $this->dbTableName . " set $sql");
  }

}
