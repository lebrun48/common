var lastModalUsed;
function jsMsgBox(content, c)
{
  var config = {setFocus: true, id: 'modalDefault', title: 'Message', bModal: true, button: [{label: 'Ok', close: true}], buttonWidth: 60, setFocus: true};
  if (c !== undefined) {
    for (var a in c) {
      config[a] = c[a];
    }
  }
  lastModalUsed = config.id;

  for (i in config.button) {
    if (!config.button[i].click) {
      config.button[i].click = '';
    } else {
      config.button[i].click = "onclick=" + config.button[i].click;
    }

    if (!config.button[i].class) {
      config.button[i].class = "btn-sm btn-primary";
    }
    if (!config.button[i].type) {
      config.button[i].type = "type=button";
    }
    if (config.button[i].close) {
      config.button[i].close = " data-dismiss='modal'";
    } else {
      config.button[i].close = '';
    }
    if (!config.dialog) {
      config.dialog = '';
    }
    if (config.modal) {
      config.modal = "data-backdrop='false'";
    } else {
      config.modal = '';
    }

  }

  line = "   <div class='modal fade addNewInputs' id=" + config.id + " tabindex='-1' role='dialog' aria-labelledby=" + config.id + " aria-hidden='true'" + config.modal + ">"
          + "  <div class='modal-dialog " + config.dialog + "' role='document'>"
          + "    <div class='modal-content'>"
          + "      <div class='modal-header text-center'>"
          + "        <h4 class='modal-title w-100 font-weight-bold text-primary ml-5'>" + config.title + "</h4>"
          + "        <button type='button' class='close text-primary' data-dismiss='modal' aria-label='Close'>"
          + "          <span aria-hidden='true'>&times;</span>"
          + "        </button>"
          + "      </div>"
          + "      <div class='modal-body mx-3'>"
          + content
          + "      </div>"
          + "      <div class='m-2 d-flex flex-row-reverse'>";
  for (i in config.button) {
    button = config.button[i];
    line += "        <button " + button.type + " " + button.click + " class='btn buttonAdd " + button.class + "'" + button.close + ">" + button.label;
    if (button.icon) {
      line += "        <i class='" + button.icon + " ml-1'></i>"
      line += "      </button>"
    }
  }
  line += "        </div>"
          + "    </div>"
          + "  </div>"
          + "</div>";

  $("#" + config.id).remove();
  $("#modals").append(line);


  $("#" + config.id).modal("show");

  setTimeout(function () {
    if (config.setFocus) {
      b = $("#" + config.id);
      f = b.find("input[type=text],input[type=number],input[type=password],input[type=radio],input[type=checkbox],textarea,select").filter(":visible:first");
      if (f) {
        f.focus();
      }
    }
  }, 500);
}


function msgBoxClose(id)
{
  if (!id) {
    id = lastModalUsed;
  }
  $("#" + id).modal("hide");
}


function msgBoxShow(content, options, id)
{
  if (!id) {
    id = "defaultModal";
  }
  lastModalUsed = id;
  $("#" + id).remove();
  $("#modals").append(content);

  if (!options) {
    options = {};
  }
  options.show = true;
  options.focus = true;

  $("#" + id).modal(options).on("shown.bs.modal", function () {
    b = $("#" + id).find("input,textarea,select").filter(":visible");
    if (b) {
      b.filter("[value]").focus();
      b.filter(":first").focus();
    }
  });
}
