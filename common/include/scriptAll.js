agent = {};
$(document).ready(function () {
//  $('.button-collapse').sideNav({
//    menuWidth: 300,
//  });
//  // SideNav Button Initialization
//  var sideNavScrollbar = document.querySelector('.custom-scrollbar');
//  var ps = new PerfectScrollbar(sideNavScrollbar);
//  new WOW().init();

  //showHideOnScroll();
  if (typeof window["entryListener"] === "function") {
    entryListener();
  }
  if ($('#startLogin').text().length) {
    login();
  }
  $(window).one('mousemove', function () {
    if (typeof agent.hasMouse === "undefined") {
      agent.hasMouse = true;
    }
  }).one('touchstart', function () {
    agent.hasMouse = false;
  });
});

firstDatePicker = true;
function addFormListener(id, callOnSubmit)
{
  $('.mdb-select').materialSelect();
  if (firstDatePicker) {
    firstDatePicker = false;
    $.extend($.fn.datepicker.defaults, {
      monthsFull: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre',
        'Novembre', 'Décembre'],
      monthsShort: ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil', 'Aou', 'Sep', 'Oct', 'Nov', 'Déc'],
      weekdaysFull: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
      weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],

      today: 'aujourd\'hui',
      clear: 'effacer',
      close: "fermer",

      labelMonthNext: 'Mois suivant',
      labelMonthPrev: 'Mois Précédent',
      labelMonthSelect: 'Sélectionnez un mois',
      labelYearSelect: 'Sélectionnez une année',

      formatSubmit: 'yyyy-mm-dd',
      format: 'dd/mm/yyyy'
    })
  }
  $('.datepicker').each(function (index, el) {
    if (el.id.indexOf("_root") !== -1) {
      el.parentNode.removeChild(el);
    } else {
      $(el).datepicker($(el).data("param"));
    }
  });

  if (id && (form = document.getElementById(id))) {
    form.addEventListener("keyup", function (event) {
      // Number 13 is the "Enter" key on the keyboard
      if (event.keyCode === 13) {
        // Cancel the default action, if needed
        event.preventDefault();
        // Trigger the button element with a click
        $(form).submit(function () {
          event.preventDefault();
          event.stopPropagation();
        });
      }
    });

    form.addEventListener('submit', function (event) {
      event.preventDefault();
      event.stopPropagation();
      if (form.checkValidity() !== false) {
        if (typeof validateBeforeSubmit === "function") {
          validateBeforeSubmit();
        }
        callOnSubmit();
      } else {
        form.classList.add('was-validated');
      }
    }, false);
  }
}

function showHideOnScroll()
{
  var lastScrollValue = 0;
  var actionCollapse = 0;
  var isCollapse = false;
  $(window).on('scroll', function () {
    var currentValue = $(window).scrollTop();
    if (actionCollapse || Math.abs(currentValue - lastScrollValue) < 20) {
      return;
    }
    if (currentValue > lastScrollValue) {
      if (!isCollapse) {
        if (actionCollapse == 0) {
          $("#beforeTable").collapse("hide");
        }
        actionCollapse = -1;
        isCollapse = true;
      }
    } else if (isCollapse) {
      if (actionCollapse == 0) {
        $("#beforeTable").collapse("show");
      }
      actionCollapse = 1;
      isCollapse = false;
    }
    lastScrollValue = currentValue;
  });

  $("#beforeTable").on('shown.bs.collapse', function () {
    $("#mainTable .group-day").css("top", 130);
    if (actionCollapse == 1) {
      actionCollapse = 0;
    } else {
      $("#beforeTable").collapse("hide");
      actionCollapse = -1;
    }
  });

  $("#beforeTable").on('hidden.bs.collapse', function () {
    $("#mainTable .group-day").css("top", 50);
    if (actionCollapse == -1) {
      actionCollapse = 0;
    } else {
      $("#beforeTable").collapse("show");
      actionCollapse = 1;
    }
  });
}

function nextPage(disabled, tableId)
{
  if (disabled) {
    return;
  }
  tableDef = tableDefinition[tableId];
  if (!tableDef.$selector) {
    tableDef.$selector = $("#" + tableId + " tr");
  }
  tableDef.$selector.slice(tableDef.pagination.begin, tableDef.pagination.end).hide();
  tableDef.pagination.begin = tableDef.pagination.end;

  searchInTable($("#search_" + tableId).val(), tableId, 1);
}

function prevPage(disabled, tableId)
{
  if (disabled) {
    return;
  }
  tableDef = tableDefinition[tableId].pagination;
  if (!tableDef.$selector) {
    tableDef.$selector = $("#" + tableId + " tr");
  }
  tableDef.$selector.slice(tableDef.pagination.begin, tableDef.pagination.end).hide();
  tableDef.pagination.begin--;

  searchInTable($("#search_" + tableId).val(), tableId, 2);
}

function middlePage(disabled, tableId)
{
  if (disabled) {
    return;
  }
  tableDef = tableDefinition[tableId].pagination;
  if (!tableDef.$selector) {
    tableDef.$selector = $("#" + tableId + " tr");
  }
  tableDef.$selector.slice(tableDef.pagination.begin, tableDef.pagination.end).hide();
  tableDef.pagination.begin = tableDef.pagination.middle;

  searchInTable($("#search_" + tableId).val(), tableId, 3);
}

function searchInTableById(id, tableBodyId, searchType)
{
  if (obj = document.getElementById(id)) {
    searchInTable(obj, tableBodyId, searchType);
  }
}


var compactForSearch = {" ": "", ":": "", ";": "", "é": "e", "\"": '', "'": '', "è": "e", "ç": "c", "à": "a", "â": "a",
  "ê": "e", "û": "u", "î": "i", "ô": "o", "ä": "a", "ë": "e", "ü": "u", "ï": "i", "ö": "o"};
function searchInTable(toSearch, tableBodyId, searchType) //0=>normal search 1=>nextPage 2=>previous page 3=>today 4=>begin 5=>end
{
  var $previousGroup = null;
  var lastVisibleNb = 0;
  var lastGroupVal = '';
  var visibleNb = 0;
  var lastVisibleIndex = 0;
  var endPagination = false;
  if (typeof searchType === "undefined") {
    searchType = 0;
  }
  var tableDef = $("#def_" + tableBodyId).data("value");
  var currentBegin = 0;
  var currentEnd = Number.POSITIVE_INFINITY;

  toSearch = toSearch.toLocaleLowerCase().replace(/[^A-Za-z0-9]/g, function (x) {
    return x in compactForSearch ? compactForSearch[x] : x;
  });
  var toSearchArray = toSearch.split(",");

  if (!tableDef.$selector) {
    tableDef.$selector = $("#" + tableBodyId + " tr");
  }

  if (hasPagination = typeof tableDef.pagination !== "undefined") {
    var pagination = tableDef.pagination;
    for (i in pagination.$toHide) {
      pagination.$toHide[i].hide();
    }
    pagination.$toHide = [];

    currentBegin = pagination.begin;
  }


//improve search
  if (toSearch
          && searchType == 0
          && toSearch.length > tableDef.previousSearch.value.length
          && toSearch.indexOf(tableDef.previousSearch.value) == 0) {
    currentBegin = tableDef.previousSearch.begin;
    currentEnd = tableDef.previousSearch.end;
  }
  tableDef.previousSearch.begin = -1;
  tableDef.previousSearch.end = Number.POSITIVE_INFINITY;

  function showToggle($elt) {
    $elt.show();
    pagination.$toHide.push($elt);
  }
  $current = tableDef.$selector.eq(currentBegin);
  if (hasPagination && $current.hasClass("group-toggle")) {
    showToggle($current.children().eq(tableDef.colGroup));
  }
  for (; $current.length; $current = searchType != 2 ? $current.next() : $current.prev()) {
    if ($current.hasClass("group-line")) {
      //manage group line

      if (searchType == 2) {
        //manage group view search up
        if (lastVisibleNb) {
          $current.show();
          lastVisibleIndex = $current.index();
          visibleNb++;
          if ($previousGroup && $previousGroup.hasClass("group-toggle")) {
            $colGroup = $previousGroup.children().eq(tableDef.colGroup);
            if ($colGroup.text() != $current.children().eq(tableDef.colGroup).text()) {
              showToggle($colGroup);
            }
          }
          $previousGroup = $current;
          if (hasPagination && visibleNb >= pagination.step) {
            if ($current.hasClass("group-toggle")) {
              showToggle($current.children().eq(tableDef.colGroup));
            }
            break;
          }

        }
        lastVisibleNb = 0;
        continue;
      }

      //group view search down
      if ($previousGroup) {
        if (lastVisibleNb) {
          visibleNb++;
          if (hasPagination) {
            //continue pagination until next valid group
            if (endPagination) {
              lastVisibleIndex = $previousGroup.index() - 1;
              break;
            }
            if (visibleNb >= pagination.step || ($current.index() - tableDef.previousSearch.start) > 200) {
              endPagination = true;
            }
          }

          $previousGroup.show();
          if (tableDef.previousSearch.begin < 0) {
            tableDef.previousSearch.begin = $previousGroup.index();
          }
          $colGroup = $previousGroup.children().eq(tableDef.colGroup);
          if ($previousGroup.hasClass("group-toggle")) {
            if ($colGroup.text() != lastGroupVal) {
              showToggle($colGroup);
            }
          }
          lastGroupVal = $colGroup.text();
        } else {
          $previousGroup.hide();
        }
      }
      $previousGroup = $current;
      lastVisibleNb = 0;
      continue;
    }

    //manage normal line
    content = $current.children().last().text();
    check = true;
    for (i in toSearchArray) {
      if (!(check = check && content.indexOf(toSearchArray[i]) > -1)) {
        break;
      }
    }
    if (check) {
      visibleNb++;
      lastVisibleNb++;
      lastVisibleIndex = $current.index();
      if (!tableDef.hasGroup && visibleNb == 1) {
        tableDef.previousSearch.begin = lastVisibleIndex;
      }
    }
    $current.toggle(check && !endPagination);
    if ($current.index() >= currentEnd) {
      break;
    }

    if (hasPagination && visibleNb >= pagination.step && !tableDef.hasGroup) {
      break;
    }
  }
  if (searchType != 2 && !lastVisibleNb && $previousGroup) {
    $previousGroup.hide();
  }
  if (hasPagination) {
    if (searchType != 2) {
      if (lastVisibleIndex < pagination.end) {
        tableDef.$selector.slice(lastVisibleIndex + 1, pagination.end).hide();
      }
      pagination.end = lastVisibleIndex + 1;
    } else {
      //searchType == 2 is only used for a page up
      pagination.end = pagination.begin + 1;
      pagination.begin = lastVisibleIndex;
    }
  }

  tableDef.previousSearch.value = toSearch;
  if (tableDef.previousSearch.begin == -1) {
    tableDef.previousSearch.begin = currentBegin;
  }
  if (!$current.length && searchType != 2) {
    //at the end
    $("#nPage_" + tableBodyId).prop("disabled", true);
    tableDef.previousSearch.end = currentEnd < Number.POSITIVE_INFINITY ? currentEnd : lastVisibleIndex;
  } else {
    $("#nPage_" + tableBodyId).prop("disabled", false);
  }
  if (hasPagination && pagination.begin) {
    //not at the beginnig
    $("#pPage_" + tableBodyId).prop("disabled", false);
  } else {
    $("#pPage_" + tableBodyId).prop("disabled", true);
  }
}

function tableAction(request, submitFunction, options)
{
  formId = "editFormId";
  axExecute('', request, function (data) {
    if (data.charAt(0) == '{') {
      genericDisplay(data);
      return;
    }
    msgBoxShow(data, options);
    listener = request.table + "EdListener";
    if (typeof window[listener] === "function") {
      window[listener]();
    }
    addFormListener(formId, function () {
      if (typeof submitFunction === "function") {
        submitFunction(formId);
      } else {
        ar = $("#" + formId).serializeArray();
        for (i in ar) {
          request[ar[i].name] = ar[i].value;
        }
        request.xAction = "submit";
        axExecute('', request);
      }
    });
  });
}


function toggleSlide(id, show, validation)
{
  $slide = $("#slide_" + id);
  if (show) {
    if (validation) {
      $("#ed_" + id).attr(validation);
    }
    $slide.slideDown(200, "swing", function () {
      $("#ed_" + id).focus();
    });
  } else {
    $element = $("#ed_" + id);
    for (key in validation) {
      $element.removeAttr(key);
    }
    $slide.slideUp(200);
  }
}

function maximize()
{
  var $div = $('#left-panel');
  $div.toggleClass('fade');
  if ($div.css("margin")) {
    $div.css("margin", '');
    $div.css("padding", "0px 12px");
  }
  setTimeout(function () {
    $div.css("margin", $("main").css("margin"));
    $div.css("padding", $("main").css("padding"));
    $div.toggleClass('fade fullscreen col-4');
  }, 200);
}

function loadPage(page)
{
  axExecute(null, {xAction: "load", page: page}, function (data) {
    if (!data) {
      return;
    }
    if (data[0] === '{') {
      genericDisplay(data);
      return;
    }
    $("main").html(data);
    listener = page.split('\\').pop().split('/').pop() + "Listener";
    if (window[listener]) {
      window[listener]();
    }
  });
}

function axExecute(action, params, callBack)
{
  if ($("#iconWait").filter(":visible").length) {
    return;
  }
  $("#iconWait").show();
  var localParam = '';
  if (params) {
    localParam = params;
  }
  if (action) {
    if (typeof localParam !== "object") {
      localParam = "xAction=" + action + (localParam.length ? '&' + localParam : '');
    } else {
      localParam.xAction = action;
    }
  }
  $.post("ajax.php", localParam, function (data) {
    $("#iconWait").hide();
    if (data) {
      first = true;
      while ((pos = data.indexOf("%+++%")) != -1) {
        if (first) {
          document.getElementById("debug_info").innerHTML = '';
          first = false;
        }
        if ((end = data.indexOf("%+++%", pos + 5)) == -1) {
          end = data.length - 5;
        }
        document.getElementById("debug_info").innerHTML += data.substr(pos + 5, end - pos - 5);
        data = data.substr(0, pos) + data.substr(end + 5);
      }
    }

    if (!data) {
      return;
    }
    if (!callBack || typeof callBack !== "function") {
      genericDisplay(data);
    } else {
      callBack(data);
    }
  });
}

function genericDisplay(data) {
  if (data.charAt(0) != '{') {
    jsMsgBox(data, {title: "ERROR"});
    return;
  }
  obj = JSON.parse(data);
  for (key in obj) {
    if (key == "executeJS") {
      param = obj[key];
      if (!Array.isArray(param)) {
        window[param]();
      } else {
        if (!Array.isArray(param[0])) {
          window[param[0]](param[1])
        } else {
          for (i in param) {
            if (param[i].length === 2) {
              window[param[i][0]](param[i][1])
            } else {
              window[param[i][0]]()
            }
          }
        }
      }
    } else {
      objJS = document.getElementById(key);
      if (objJS) {
        next = objJS.nextElementSibling;
        parent = objJS.parentElement;
        parent.removeChild(objJS);
        if (obj[key]) {
          var d = document.createElement('div');
          d.innerHTML = obj[key];
          doc = d.firstChild;
          if (!next) {
            parent.appendChild(doc);
          } else {
            parent.insertBefore(doc, next);
          }
        }
      }
    }
  }
}

function tableListener(table)
{
  classes = '';
  $(table + " .tr-hover").hover(function () {
    $action = $(this).find(".td-action-icon");
    if ($action.length) {
      $td = $action.parent();
      classes = $td.attr("class");
      $td.toggleClass("text-nowrap text-truncate", true);
      $action.show(agent.hasMouse ? 0 : 100, function () {
        $(this).click(function () {
          request = {xAction: "edit", action: $(this).data("action"), table: table.substr(1), primary: $(this).parents("tr").data("primary")};
          tableAction(request);
        });
        $(table + " .td-update").click(function () {
          request = {xAction: "edit", action: $(this).data("action"), table: table.substr(1), primary: $(this).parent().data("primary")};
          tableAction(request);
        });
      });
    } else {
      $(table + " .td-update").click(function () {
        request = {xAction: "edit", action: $(this).data("action"), table: table.substr(1), primary: $(this).parent().data("primary")};
        tableAction(request);
      });

    }
    $(this).toggleClass("z-depth-1", true);
  }, function () {
    $(this).toggleClass("z-depth-1", false);
    $(this).find(".td-action-icon").hide().off("click").parent().attr("class", classes);
    $(table + " .td-update").off("click");
  });


  $(table + " .td-edit").click(function () {
    $(this).parents("table").find("input:visible").each(function () {
      $parent = $(this).parent();
      if ($parent.is("td")) {
        toggleNewLine($parent.parent(), false);
      } else {
        $parent.hide();
        $text = $parent.next();
        $text.show();
      }
    });
    $input = $(this).children().first();
    $text = $input.next();
    $text.hide();
    $input.show().children().first().focus().val($text.text());
  });

  $(table + " input").keyup(function (event) {
    if (event.keyCode === 27) {
      $parent = $(this).parent();
      if ($parent.is("td")) {
        toggleNewLine($parent.parent(), false);
        return;
      }
      $text = $parent.next();
      $parent.hide();
      $text.show();
      return;
    }
    if (event.keyCode === 13) {
      $(this).next().click();
    }

  });

  $(table + " .td-action-validate").click(function () {
    action = (primary = $(this).parents("tr").data("primary")) ? "update" : "insert";
    request = {xAction: "edit", action: action, table: table.substr(1), primary: primary};
    request[$(this).prev().attr("name")] = $(this).prev().val();
    tableAction(request);
  });

}

function insertTable(table)
{
  $(table + " input:visible").each(function () {
    $parent = $(this).parent();
    if (!$parent.is("td")) {
      $parent.hide();
      $text = $parent.next();
      $text.show();
    }
  });
  toggleNewLine($(table + " .new-line"), true);
}

function toggleNewLine($nl, show)
{
  if ($nl.length) {
    if (show) {
      if ($nl.filter(":visible").length) {
        return;
      }
      $nl.addClass($nl.data("attr")).show();
      $nl.find("input").first().focus();
    } else if ($nl.filter(":visible").length) {
      $nl.removeClass($nl.data("attr")).hide();
    }
  }
}