<?php

// ->bordered attribute
define("TABLE_BORDER_DEFAULT", 0);
define("TABLE_BORDER_FULL", 1);
define("TABLE_BORDER_NONE", 2);

define("TABLE_ALLOW_INSERT", 1);
define("TABLE_ALLOW_EDIT", 2);
define("TABLE_ALLOW_DELETE", 4);
define("TABLE_ALLOW_ALL", 7);
define("TABLE_ALLOW_ONLY_DISPLAY", 8);

//"title", 
define("COL_TITLE", 0);
//"db colonne name" default=key 
define("COL_DB", 1);
//true  default=false
define("COL_PRIMARY", 2);
//"money" | "img[attribute]" | "date[format]",
define("COL_TYPE", 3);
//"attr list" attribute list (class, style, ...),
define("COL_TH_ATTR", 4);
//"attr list",
define("COL_TD_ATTR", 5);
//"true" if set => no db colonne for this key
define("COL_NO_DB", 6);
//"immediate value" may contain other key value use §key§ to put the key value in the defined place
define("COL_VALUE", 7);
//true  not include this column in the search
define("COL_NO_SEARCH", 8);
//"check[value]" | "list" | "input" // indicat which type of filter to build
define("COL_FILTER_TYPE", 9);
//Contains the column index (set by the system), do not set
define("COL_INDEX", 10);
//position table ordered followig position. The line with grouped value is generated on change of any value
define("COL_GROUP", 11);
//"asc" or "desc" default = "asc"
define("COL_GROUP_ORDER", 12);
//table ordered by value and hide if same as previous value;
define("COL_HIDE_SAME", 13);
//colonne will not be generated unless COL_PRIMARY;
define("COL_HIDDEN", 14);
//colonne that may be click on. Have td_<tableId>_<key> class added;
define("COL_LISTENER", 15);
//colonne can be edited online
define("COL_EDIT", 16);

include_once ("common/DBUtil.php");

class BuildTable
{

  use DBUtil;

//display attributes
  public $colDefinition;
//array(key_name=>array(
//
//display table
  public $isStriped = true;
  public $bordered = TABLE_BORDER_DEFAULT;
  public $surroundTableAttr;
  public $pagination = 0;  //set a value to see the number of lines in pagination mode
  public $isSmall = true;
  public $hasSelect = false;
  public $noHeader = false;
  public $stickyHeader = false;
  public $viewLineNumber = false;
  public $enableSearch; //to enable search put this value with the ID elemnt of the search
  public $searchContent; //content the search value for the current line without any special character. The column value are separated by a comma.
  public $filterId; //to enable filter and with id of filters definition
  public $filterContent;
  public $rightSearchContent; // search take 50% of table width on left, right space can be used by etting this variable.
  public $tableId;
  public $theadAttr; //thead attribute
  public $beforeTableAttr = "id=beforeTable class='before-table'"; //attribute to add at div before table by default is fixed pos with 
  public $allowModification;
  public $allowEdit;
  public $insertInline;
//DataBase
  public $tableContent;  // if no DB, this value can be the table content
  public $dbTableName;
  public $otherDbCol;
  public $defaultOrderFirst;
  public $defaultOrderLast;
  public $defaultWhere;
  //private
  private $filter = array();
  private $lastGroupValue;
  private $lineNumber = 0;
  private $row;
  private $beginPagination;
  private $endPagination;
  private $order;
  private $paginationLineStatus = 0; //=>hide, 1=>view until next group line 2=>hide again (page terminated)
  private $shouldStopPagination = false;
  private $hasGroup = false;
  private $tableBodyId; // mainly for search operation
  private $primaryCols;
  private $firstVisible;

  function __construct($table)
  {
    global $request;
    if (is_array($table)) {
      $this->noDBTable = true;
      $this->tableContent = $table;
    } else {
      $this->dbTableName = $table;
    }
  }

  function buildFullTable()
  {
    $this->initMembersAndExecuteAction();
    $this->buildBeforeTable();
    $this->buildTable();
    $this->buildAfterTable();
  }

  private function initMembersAndExecuteAction()
  {
    global $action;
    if (!$this->dbTableName) {
      $this->dbTableName = "mainTable";
    }
    if (!$this->tableId) {
      $this->tableId = strtok($this->dbTableName, ', ');
    }
    $this->tableBodyId = "body" . ucfirst($this->tableId);
    switch ($action) {
      case "insert":
        $this->insertRow();
        break;

      case "update":
        $this->updateRow();
        break;

      case "delete":
        $this->deleteRow();
        break;
    }
  }

  function buildBeforeTable()
  {
    echo "<div " . addAttribute($this->beforeTableAttr, "d-flex justify-content-between") . ">";
    if ($this->enableSearch) {
      $this->buildSearch();
    }
    if ($this->pagination) {
      $this->buildPagination();
    }
    if ($this->allowEdit & TABLE_ALLOW_INSERT || $this->insertInline) {
      echo '<a type="button" href=javascript:insertTable("#' . $this->tableId . '") class="btn-floating btn-sm ' . BRAND_COLOR . '"><i class="fas fa-plus" aria-hidden="true"></i></a>';
    }
    echo "</div>";
    if ($this->surroundTableAttr) {
      echo "<div $this->surroundTableAttr>";
    }
  }

  function buildSearch()
  {
    echo ""
    . "<div class='input-group md-form form-sm form-1 mt-0 w-50'>"
    . " <div class='input-group-prepend'>"
    . "  <span class='input-group-text " . BRAND_COLOR . " lighten-3'><i class='fas fa-search text-white' aria-hidden='true'></i></span>"
    . " </div>"
    . " <input id='search_" . $this->tableBodyId . "' onchange=this.blur();searchInTable(this.value,'" . $this->tableBodyId . "') class='form-control my-0 py-1' type='text' placeholder='Chercher...'>"
    . "</div>"
    . "<div class=pt-3>" . $this->rightSearchContent . "</div>\n";
  }

  function buildPagination()
  {
    echo ""
    . "<div class='pg-pagination' style=vertical-align:middle>"
    . " <a id='pPage_" . $this->tableBodyId . "' class='prev page-numbers' href=javascript:prevPage(this,'" . $this->tableBodyId . "')>" . $this->getPgPrevContent() . "</a>";
    for ($i = 0; !$end; $i++) {
      $c = $this->getPgmiddleContent($i, $end, $isCurrent);
      echo ""
      . " <a class='page-numbers" . ($isCurrent ? " current" : '') . "' href=javascript:middlePage(this,'" . $this->tableBodyId . ",$i')>$c</a>";
    }
    echo ""
    . " <a id='nPage_" . $this->tableBodyId . "' class='next page-numbers' href=javascript:prevPage(this,'" . $this->tableBodyId . "')>" . $this->getPgNextContent() . "</a>"
    . "</div>\n";
  }

  function getPgPrevContent()
  {
    return "Prev";
  }

  function getPgMiddleContent($i, &$end, &$isCurrent)
  {
    $end = true;
    return "middle";
  }

  function getPgNextContent()
  {
    return "Next";
  }

  function buildTable()
  {
    if (!$this->tableBodyId) {
      $this->initMembersAndExecuteAction();
    }
    $this->tableBodyId = "body" . ucfirst($this->tableId);
    if ($this->isStriped) {
      $class .= " table-striped";
    }
    switch ($this->bordered) {
      case 1: $class .= " table-bordered";
        break;
      case 2: $class .= " table-borderless";
        break;
    }
    if ($this->isSmall) {
      $class .= " table-sm";
    }
    echo ""
    . "<table class='table$class' id=$this->tableId>";

    $this->buildHeader();
    $this->buildBody();
  }

  function buildHeader()
  {
    if ($this->noHeader) {
      $this->theadAttr = addAttribute($this->theadAttr, "d-none");
    }
    echo ""
    . "<thead " . $this->theadAttr . ">"
    . "  <tr " . $this->getTrAttributes() . ">"
    . "    <th" . (!$this->viewLineNumber ? " class='d-none'" : '') . ">#</th>";
    $nb = 0;
    foreach ($this->colDefinition as $k => $v) {
      if (isset($v[COL_GROUP])) {
        $this->hasGroup = true;
      }
      $this->colDefinition[$k][COL_INDEX] = $nb;
      if ($v[COL_PRIMARY]) {
        $this->primaryCols[] = $nb;
      }
      echo ""
      . "    <th " . (isset($v[COL_GROUP]) ? addAttribute($v[COL_TH_ATTR], "group-" . $v[COL_GROUP]) : $v[COL_TH_ATTR]) . ">" . $v[COL_TITLE] . "</th>";
      $nb++;
    }
    if (!$this->noHeader) {
      echo ""
      . "  </tr>"
      . "</thead>\n";
    }
  }

  function getTrAttributes($row = null) //row is null for header line
  {
    
  }

  function buildBody()
  {
    echo "<tbody id=$this->tableBodyId>";
    if ($this->insertInline) {
      $out = extractAttr($this->getTrAttributes(), "class");
      echo "<tr style=display:none class=new-line" . ($out[2][0] ? " data-attr='" . $out[5][0] . "'" : '') . ">";
      foreach ($this->colDefinition as $key => $attr) {
        if ($attr[COL_HIDDEN])
          continue;
        echo "<td " . $attr[COL_TD_ATTR] . ">";
        if ($attr[COL_EDIT]) {
          echo "<input name=val_$key>" . $this->getActionIcon("validate");
        }
        echo "</td>";
      }
      echo "</tr>";
    }
    if (isset($this->tableContent)) {
      foreach ($this->tableContent as $this->row) {
        $this->buildLine($this->row);
      }
    } else {
      $sql = $this->buildSql();
      $res = jmysql_query($sql);
      while ($this->row = jmysql_fetch_assoc($res)) {
        $this->buildLine($this->row);
      }
    }
    echo "</tbody>";
  }

  function initSearchContent($row)
  {
    $this->searchContent = '';
  }

  function initFilterContent($row)
  {
    $this->filterContent = '';
  }

  function buildLine($row)
  {
    if ($this->hasGroup) {
      $this->buildGroup($row);
    }
    $this->buildheadLine($this->getTrAttributes($row));
    if ($this->enableSearch) {
      $this->initSearchContent($row);
    }
    $this->initFilterContent($row);


    foreach ($this->colDefinition as $key => $attr) {
      if ($attr[COL_HIDDEN]) {
        continue;
      }
      if (!isset($attr[COL_GROUP]) && !$this->firstVisible) {
        $this->firstVisible = $key;
      }
      $this->buildColumn($key, $row, $attr);
    }
    if ($this->enableSearch || $this->filterContent) {
      echo "<td class=d-none>" . substr($this->searchContent, 1);
      echo ";" . $this->filterContent . "</td>";
    }
    echo "</tr>\n";
  }

  private function buildHeadLine($attr, $forGroup = false)
  {
    if ($this->pagination) {
      if ($this->paginationLineStatus == 0 && $this->startPaginationView($this->row, $this->lineNumber)) {
        $this->beginPagination = $this->lineNumber;
        $this->paginationLineStatus = 1;
      }

      if (!$this->isLineVisible($forGroup)) {
        $attr = addAttribute($attr, "display:none;", "style");
      }
    }

    $allowEdit = $this->allowEditLine($this->row);
    if (!$forGroup && $allowEdit) {
      $arrKeys = array_keys($this->colDefinition);
      foreach ($this->primaryCols as $idx) {
        $primary[] = array($idx, $this->row[$arrKeys[$idx]]);
      }
    }
    echo "<tr " . (!$forGroup && $allowEdit ? addAttribute($attr, "tr-hover") . " data-primary=" . json_encode($primary) : $attr) . ">";

    if ($this->viewLineNumber) {
      echo "<td class='text-right'>" . $this->lineNumber . "</td>";
    }
    $this->lineNumber++;
  }
  
  function allowEditLine($row)
  {
    return $this->allowEdit;
  }

  function startPaginationView($row, $lineNumber)
  {
    return true;
  }

  private function isLineVisible($isGroupLine)
  {
    if ($this->paginationLineStatus == 1) {
      if ($this->shouldStopPagination && $isGroupLine) {
        $this->endPagination = $this->lineNumber;
        $this->paginationLineStatus = 2;
        return false;
      }
      if (($tmp = $this->lineNumber - $this->beginPagination) && is_int($tmp / $this->pagination)) {
        if ($this->hasGroup && !$isGroupLine) {
          return $this->shouldStopPagination = true;
        }
        $this->endPagination = $this->lineNumber;
        $this->paginationLineStatus = 2;
        return false;
      }
      return true;
    }
    return false;
  }

  function buildColumn($key, $row, $attr)
  {
    $ret = $this->formatDisplayValue($key, $row, $attr);
    echo "<td " . (isset($attr[COL_GROUP]) ? addAttribute($ret->attr, "group-" . $attr[COL_GROUP]) : $ret->attr) . ">" . $ret->value . "</td>";
  }

  function formatDisplayValue($key, $row, $attr)
  {
    $allowEdit = $this->allowEditLine($this->row);
    $value = $this->getDisplayValue($key, $row);
    if (strlen($row[$key]) && $attr[COL_FILTER_TYPE]) {
      $this->getFilterContent($key, $row, $attr);
    }
    foreach ($attr as $k => $format) {
      switch ($k) {
        case COL_TYPE :
          $format = strtok($format, "[ ");
          $param = strtok(']');
          switch ($format) {
            case "date":
              $date = new DateTime($value);
              if (!$param) {
                $param = "d/m/Y H:i:s";
              }
              $value = formatDate($date, $param);
              break;

            case "money":
              $class .= "money-" . ($value >= 0 ? "pos" : "neg") . " text-right";
              $value = getMoney($value);
              break;

            case "img":
              $value = "<img " . $param . " src='$value' alt='" . $attr[COL_TITLE] . "'>";
              break;
          }
          break;

        case COL_VALUE:
          $value = $this->translateKeyValue($attr[$k], $row);
          break;

        case COL_LISTENER:
          $class .= " td-listener";
          $attr[COL_TD_ATTR] = addAttribute($attr[COL_TD_ATTR], $key, "data-key");
          break;

        case COL_EDIT:
          if ($allowEdit & TABLE_ALLOW_EDIT) {
            $class .= " td-edit";
            $value = "<span style=display:none><input name=val_$key>" . $this->getActionIcon("validate") . "</span><span>$value</span>";
          }
          break;
      }
    }
    if ($this->enableSearch && strlen($value) && !$attr[COL_NO_SEARCH]) {
      $this->searchContent .= "," . $this->compactForSearch($value);
    }

    if ($allowEdit && !$attr[COL_EDIT]) {
      if ($this->firstVisible == $key && $allowEdit & TABLE_ALLOW_DELETE) {
        $value = $this->getActionIcon("delete") . $value;
      } else if (!$attr[COL_LISTENER] && $allowEdit & TABLE_ALLOW_EDIT) {
        $attr[COL_TD_ATTR] = addAttribute($attr[COL_TD_ATTR], "td-update");
        $attr[COL_TD_ATTR] = addAttribute($attr[COL_TD_ATTR], $allowEdit & TABLE_ALLOW_ONLY_DISPLAY ? "display": "update", "data-action");
      }
    }

    $return = new stdClass();
    $return->attr = addAttribute($this->getTdAttr($key, $row, $attr[COL_TD_ATTR]), $class);
    $return->value = $value;
    return $return;
  }

  function getActionIcon($action)
  {
    switch ($action) {
      case "delete":
        return '<span style="display:none" class=td-action-icon  data-action=' . $action . '><i class="far fa-trash-alt fa-lg"></i></span>';
      case "update":
        return '<span style="display:none" class=td-action-icon data-action=' . $action . '><i class="fas fa-pencil-alt></i></span>';
      case "validate":
        return '<span class=td-action-validate data-action=' . $action . '><i class="fas fa-check"></i></span>';
    }
  }

  function getTdAttr($key, $row, $attr)
  {
    return $attr;
  }

  function buildGroup($row)
  {
    $atLeastOne = false;
    foreach ($this->colDefinition as $key => $attr) {
      if (isset($attr[COL_GROUP])) {
        if (!$atLeastOne && ($ne = $row[$key] != $this->lastGroupValue[$key]["val"])) {
          $atLeastOne = true;
        }
        $this->lastGroupValue[$key] = array("val" => $row[$key], "nb" => $attr[COL_GROUP], "ne" => $ne);
      }
    }

    if ($atLeastOne) {
      //group line has to appear
      foreach ($this->lastGroupValue as $key => $v) {
        $def = $this->colDefinition[$key];
        $attr = $def[COL_TD_ATTR];
        if (!$first && $def[COL_HIDE_SAME] && !$v["ne"]) {
          $first = true;
          $attr = addAttribute($attr, "display:none", "style");
          $groupToggle = " group-toggle";
        }
        $line .= "<td $attr>" . $this->formatDisplayValue($key, $row, $def)->value . "</td>";
      }
      $this->buildHeadLine(addAttribute($this->getTrAttributes($row), "group-line$groupToggle"), true);
      echo $line;
      echo "</tr>\n";
    }
  }

  public function getFilterContent($key, $row, $attr)
  {
    $this->filterContent .= ",c" . $attr[COL_INDEX] . ":" . $this->compactForSearch($row[$key]);
  }

  private function translateKeyValue($line, $row)
  {
    $value = strtok($line, '§');
    $tok = strtok('§');
    while ($tok) {
      $value .= $row[$tok] . strtok('§');
      $tok = strtok('§');
    }
    return $value;
  }

  private function compactForSearch($value)
  {
    static $translate = array(
        " "  => "", ","  => "", ":"  => "", ";"  => "", "é"  => "e", "\"" => '', "'"  => '', "è"  => "e", "ç"  => "c", "à"  => "a", "â"  => "a",
        "ê"  => "e", "û"  => "u", "î"  => "i", "ô"  => "o", "ä"  => "a", "ë"  => "e", "ü"  => "u", "ï"  => "i", "ö"  => "o");

    $s = str_replace(array_keys($translate), $translate, mb_strtolower($value));
    $pos = strpos($s, "<");
    while ($pos !== false) {
      if (($end = strpos($s, '>')) <= $pos) {
        $pos = strpos($s, '<', $pos + 1);
        continue;
      }
      $s = substr($s, 0, $pos) . substr($s, $end + 1);
      $pos = strpos($s, "<");
    }
    return $s;
  }

  function getDisplayValue($key, $row)
  {
    return $row[$key];
  }

  function buildAfterTable()
  {
    echo "</table>";
    $tableDef = array("hasGroup" => $this->hasGroup, "colGroup" => $this->viewLineNumber + 0, "\$selector" => null, "previousSearch" => array("begin" => 0, "end" => 0x7fFFFF, "value" => ''));
    if ($this->pagination) {
      $tableDef["pagination"] = array("step" => $this->pagination, "begin" => $this->beginPagination, "end" => $this->endPagination);
    }
    echo "<span id='def_" . $this->tableBodyId . "' class=d-none data-value='" . json_encode($tableDef) . "'></span>";
    if ($this->surroundTableAttr) {
      echo "</div>";
    }
  }

  function buildSql()
  {
    $sql = "select ";
    foreach ($this->colDefinition as $key => $def) {
      if (!$def[COL_NO_DB]) {
        $sql .= $this->getDbCol($key, true) . ", ";
      }
    }
    if ($this->otherDbCol) {
      $sql .= $this->otherDbCol;
    } else {
      $sql = substr($sql, 0, -2);
    }
    $sql .= " " . $this->getDbFrom();
    $sql .= " " . $this->getDbWhere();
    $sql .= " " . $this->getDbOrder();
    return $sql;
  }

  function getDbFrom()
  {
    return "from $this->dbTableName";
  }

  function getDbWhere()
  {
    $sql = "where ";
    if ($this->defaultWhere) {
      $sql .= "$this->defaultWhere and ";
    }
    foreach ($this->filter as $key => $value) {
      $dbCol = $this->getDbCol($key);
      if (is_array($value)) {
        $sql .= "(";
        foreach ($value as $v) {
          $sql .= "$dbCol" . $this->getDbFilter($key, "=$v") . " or ";
        }
        $sql = substr($sql, 0, -4) . ")";
      } else {
        $sql .= "$dbCol" . $this->getDbFilter($key, $v) . " and ";
      }
    }
    return "{$sql}1";
  }

  function getDbOrder()
  {
    $this->order = $this->defaultOrderFirst;
    foreach ($this->colDefinition as $key => $def) {
      if (isset($def[COL_GROUP]))
        $this->order .= "," . $this->getDbCol($key) . " " . $this->colDefinition[$key][COL_GROUP_ORDER];
    }
    if ($this->defaultOrderLast) {
      $this->order .= ",$this->defaultOrderLast";
    }
    $this->order = $this->order[0] == ',' ? "order by " . substr($this->order, 1) : '';
    return $this->order;
  }

  private function getDbCol($key, $addAs = false)
  {
    return ($tmp = $this->colDefinition[$key][COL_DB]) ? $tmp . ($addAs ? "as $key" : '') : $key;
  }

}
