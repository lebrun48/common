<?php
/* * *************************************************************************************************************
 *
 * To create a link in othrs projects execute the command with powershell in the project (make a symbolik link):
 * new-item -itemtype symboliclink -path . -name common -value ..\common\common
 *
 * ************************************************************************************************************* */
header('Set-Cookie: SameSite=None; Secure');
if (!$bNoDBCnx) {
  include "connectDB.php";
}
define('NL', "<br>");
define('nl', "\n");
define("AJ_FUNCTION", "function");
define("AJ_PARAM", "param");
//$seeRequest=true;
$now = new DateTime();

$userAgent = $_SERVER["HTTP_USER_AGENT"];
if ($t = stripos($userAgent, 'Firefox/'))
  $browserName = 'firefox';
elseif ($t = stripos($userAgent, 'MSIE/'))
  $browserName = 'ie';
elseif ($t = stripos($userAgent, 'Trident/'))
  $browserName = 'ie';
elseif ($t = stripos($userAgent, 'Edge/'))
  $browserName = 'edge';
elseif ($t = stripos($userAgent, 'Chrome/'))
  $browserName = 'chrome';
elseif ($t = stripos($userAgent, 'Safari/'))
  $browserName = 'safari';
if ($t) {
  $browserVersion = strtok(substr($userAgent, $t), '/');
  $browserVersion = intval(strtok('.'));
}

$isSmartphoneEngine = strpos($userAgent, "Android") || strpos($userAgent, "Symbian") || strpos($userAgent, "Windows Phone") || strpos($userAgent, "iOS") || strpos($userAgent, "Blackberry");

$request = array_merge($_GET, $_POST);
$action = $request["action"];
$xAction = $request["xAction"];
$isAjaxTest = false;
$relativeCommonPath;

if (get_magic_quotes_gpc())
  foreach ($request as $k => $v)
    if (strlen($v))
      $request[$k] = stripslashes($v);

if (!$bNoSession) {
  session_start();
}
if ($dirScript = $_SERVER["SCRIPT_URL"]) {
  if ($dirScript[$t = strlen($dirScript) - 1] != '/')
    $dirScript = dirname($dirScript);
  if ($dirScript[$t] == '/')
    $dirScript = substr($dirScript, 0, -1);
}

error_reporting(E_ERROR | E_PARSE | E_WARNING);
if (!$serverConfig->isDevPhase) {
  ini_set('display_errors', 0);
  set_error_handler("errorHandler", E_PARSE | E_COMPILE_ERROR | E_RECOVERABLE_ERROR | E_ERROR | E_CORE_ERROR);
}
if (file_exists("init.php")) {
  include "init.php";
}
if (!$bNoDBCnx) {
  include "common/mysqlFunctions.php";
  $currentDBLink = connectDB();
  $bFraudPresent = jmysql_num_rows(jmysql_query("show tables like 'fraud'"));
}
if (function_exists("InitRead"))
  InitRead();

if ($bFraudPresent && jmysql_result(jmysql_query("select count(*) from fraud where ip='" . $_SERVER['REMOTE_ADDR'] . "' and date='" . $now->format("Y-m-d") . "' and block=1"), 0)) {
  jmysql_query("update fraud set n=n+1 where ip='" . $_SERVER['REMOTE_ADDR'] . "' and date='" . $now->format("Y-m-d") . "'");
  header("Status: 404 Not Found", true, 404);
  ?>
  <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
  <html><head>
      <title>404 Not Found</title>
    </head><body>
      <h1>Not Found</h1>
      <p>The requested URL <?php echo $_SERVER["SCRIPT_URI"] ?> was not found on this server.</p>
      <hr>
      <address><?php echo $_SERVER["SERVER_SIGNATURE"] ?></address>
    </body></html>
  <?php
  exit();
}

//call $functionCall method or object with $params and return echoed line in a string
function getOutput($functionCall, &$params = null)
{
  global $isAjaxTest, $debugLogLine;
  ob_start(null, 0, PHP_OUTPUT_HANDLER_CLEANABLE | PHP_OUTPUT_HANDLER_REMOVABLE);
  if (!is_callable($functionCall, true)) {
    stop(__FILE__, __LINE__, dump($functionCall, "functionCall", 2));
    return ob_get_clean();
  }

  if (is_array($functionCall)) {
    if (isset($params)) {
      $functionCall[0]->{$functionCall[1]}($params);
    } else {
      $functionCall[0]->{$functionCall[1]}();
    }
  } else {
    if ($params) {
      $functionCall($params);
    } else {
      $functionCall();
    }
  }
  if ($isAjaxTest) {
    file_put_contents("test/ajaxOutput.html", ob_get_contents());
  }
  if ($debugLogLine) {
    $ret = ob_get_clean();
    echo $debugLogLine;
    unset($debugLogLine);
    return $ret;
  }
  return ob_get_clean();
}

define("CURRENT_PAGE", "currentPage");

function getCurrentPage($defaultPage)
{

  global $serverConfig;
  if ($serverConfig->isDevPhase || !($page = $_SESSION[CURRENT_PAGE]) || !file_exists(getcwd() . "/$page")) {
    $page = $defaultPage;
  }
  $_SESSION[CURRENT_PAGE] = $page;
  return $page;
}

function setCurrentPage($page)
{
  $_SESSION[CURRENT_PAGE] = $page;
}

function includeFile($file, $always = false)
{
  global $serverConfig;
  if ($serverConfig->isDevPhase || $always) {
    if ($serverConfig->isDevPath) {
      $file = ($file[0] == '/' ? $serverConfig->projectPath . "/www/include$file" : getcwd() . "/www/include/$file");
    } else {
      $file = ($file[0] == '/' ? $_SERVER["DOCUMENT_ROOT"] . $serverConfig->baseWww . "/include$file" : $serverConfig->browserWww . "/include/$file");
    }
    if (!file_exists($file)) {
      return;
    }
    switch (pathinfo($file, PATHINFO_EXTENSION)) {
      case "css" :
        echo "\n<style>\n";
        include $file;
        echo "</style>\n";
        break;

      case "js":
        echo "\n<script>\n";
        include $file;
        echo "</script>\n";
        break;
    }
    return;
  }

  $file = ($file[0] == '/' ? $serverConfig->baseWww . "/include$file" : "include/$file");
  switch (pathinfo($file, PATHINFO_EXTENSION)) {
    case "css" :
      echo "<link rel='stylesheet' href='$file'>\n";
      break;

    case "js" :
      echo "<script type='text/javascript' src='$file'></script>\n";
      break;
  }
  return;
}

function roundMoney($val)
{
  return (($tmp = abs(($valAbs = round(abs($val) * 100, 0)) % 5)) < 3 ? ($valAbs - $tmp) / 100 : ($valAbs + 5 - $tmp) / 100) * ($val < 0 ? -1 : 1);
}

function getMoney($val, $withEuro = true, $emptyIf0 = false)
{
  if (!$val && $emptyIf0) {
    return '';
  }
  return number_format($val, 2, ",", ".") . ($withEuro ? "€" : "");
}

//---------------------------------------------------------------------------------------------------------------------------
function checkBrowserVersion($b)
{
  global $browserName, $browserVersion;
  foreach ($b as $k => $v) {
    $name = strtok($v, '.');
    $ver = strtok(' ');
    if ($name == $browserName)
      return !strlen($ver) || $browserVersion >= $ver;
  }
  return false;
}

//-------------------------------------------------------------------------------------------------    
function errorHandler($errno, $errstr, $errfile, $errline)
{
  //echo "<pre>errno=$errno\nerrstr=$errstr\nerrfile=$errfile\nerrline=$errline</pre>";
  if (!(error_reporting() & $errno))
  // This error code is not included in error_reporting
    return;

  mail("lebrun48@gmail.com", "!! Error PHP", "errno=$errno\nerrstr=$errstr\nerrfile=$errfile\nerrline=$errline\n" . getErrorDump());
  die("<br><div style='border:2px solid red;font-size:x-large'>Un problème technique est survenu.<br>Une solution sera apportée rapidement.<br>Veuillez réessayer plus tard.</div>");
}

//-------------------------------------------------------------------------------------------------
function SendError($com = null)
{
  global $bFraudPresent;
  if ($bFraudPresent) {
    $r = jmysql_query("select n,date from fraud where ip='" . $_SERVER['REMOTE_ADDR'] . "' and date='" . $now->format('Y-m-d') . "'");
    if (jmysql_num_rows($r)) {
      if ($com == "Fraude")
        jmysql_query("update fraud set block=1 where ip='" . $_SERVER['REMOTE_ADDR'] . "' and date='" . $now->format('Y-m-d') . "'");
      else {
        if ($com != "Fraude" && jmysql_result($r, 0) < 20)
          jmysql_query("update fraud set n=n+1 where ip='" . $_SERVER['REMOTE_ADDR'] . "' and date='" . $now->format('Y-m-d') . "'");
        else
          jmysql_query("update fraud set block=1 where ip='" . $_SERVER['REMOTE_ADDR'] . "' and date='" . $now->format('Y-m-d') . "'");
      }
      if (jmysql_result($r, 0) > 2)
        return false;
    } else
      jmysql_query("insert into fraud (ip,date,n,block) values ('" . $_SERVER['REMOTE_ADDR'] . "','" . $now->format('Y-m-d') . "',1," . ($com == "Fraude" ? "1)" : "0)"));
  }
  return true;
}

//-------------------------------------------------------------------------------------------------    
function stop($file, $line, $com, $overrideMsg = null, $bNotFound = false)
{
  global $serverConfig, $request, $now, $bFraudPresent;
  if ($serverConfig->isDevPhase) {
    debugLog("<b>" . $file . '.' . $line . "</b> : $com $overrideMsg");
    return;
  }

  if (SendError($com)) {
    $t = "ID: " . $request["id"] . "\nFile:$file\nLine:$line\nComment:$com\n";
    mail("lebrun48@gmail.com", "!! Error PHP", "$t\n" . getErrorDump());
  }

  if ($bNotFound) {
    header("Status: 404 Not Found", true, 404);
    ?>
    <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
    <html><head>
        <title>404 Not Found</title>
      </head><body>
        <h1>Not Found</h1>
        <p>The requested URL <?php echo $_SERVER["SCRIPT_URI"] ?> was not found on this server.</p>
        <hr>
        <address><?php echo $_SERVER["SERVER_SIGNATURE"] ?></address>
      </body></html>
    <?php
    exit();
  }
}

//-------------------------------------------------------------------------------------------------    
function getErrorDump()
{
  global $request;
  $t = $_SERVER["HTTP_FROM"];
  $t1 = $_SERVER["HTTP_USER_AGENT"];
  if (stripos($t, "bingbot") !== false || stripos($t, "googlebot") !== false || stripos($t1, "ContextAd Bot") !== false) {
    header("Status: 404 Not Found", true, 404);
    ?>
    <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
    <html><head>
        <title>404 Not Found</title>
      </head><body>
        <h1>Not Found</h1>
        <p>The requested URL <?php echo $_SERVER["SCRIPT_URI"] ?> was not found on this server.</p>
        <hr>
        <address>Apache/1.3.41 (Unix) Server</address>
      </body></html>
    <?php
    exit();
  }
  return dump(debug_backtrace(), "stack", 2) . dump($request, "request", 2) . dump($_SESSION, "session", 2) . dump($_SERVER, "server", 2);
}

//-------------------------------------------------------------------------------------------------
function getRefreshCnt($bComplete = false)
{
  global $currentPage;
  return $_SESSION["pages"][$currentPage];
}

//-------------------------------------------------------------------------------------------------    
function toHex(&$v)
{
  for ($i = 0; $i < strlen($v); $i++)
    $s .= sprintf("%02x", ord($v[$i]));
  echo "$v $s" . NL;
  return $s;
}

//-------------------------------------------------------------------------------------------------    
function toAsc(&$v)
{
  for ($i = 0; $i < strlen($v); $i += 2)
    $s .= ord(sscanf("%02x", $v[$i]));
  echo "$v $s" . NL;
  return $s;
}

function debugLog($msg, $title = 'value', $forceDisplay = false)
{
  global $xAction, $debugLogLine;
  if ($xAction) {
    $flag = "%+++%";
    if (!$forceDisplay && ob_get_level() > 1) {
      ob_start(null, 0, PHP_OUTPUT_HANDLER_CLEANABLE | PHP_OUTPUT_HANDLER_REMOVABLE);
    }
  }
  echo $flag;
  dump($msg, time() . ": $title" . ($xAction ? "[ajax:$xAction]" : ''));
  echo $flag;
  if (!$forceDisplay && ob_get_level() > 1) {
    $debugLogLine .= ob_get_clean();
  }
}

//-------------------------------------------------------------------------------------------------    
//rectype = 0 => output
//rectype = 1 => to DB
//rectype = 2 => to string
function dump($obj, $com = "''", $recType = 0)
{
  global $serverConfig, $now;
  switch ($recType) {
    case 1:
    case 2:
      ob_start();
      break;

    case 0:
      if (!$serverConfig->isDevPhase) {
        return;
      }
      ob_start();
      break;
  }

  echo "<pre>$com=";
  var_dump($obj);
  echo "</pre>";
  $l = ob_get_contents();
  ob_end_clean();
  switch ($recType) {
    case 1:
      if (!$serverConfig->isDevPhase) {
        jmysql_query("insert into tracing set date=now() " . (isset($com) ? ",com='" . addslashes($com) . "'" : '') . ", content='" . addslashes($l) . "'");
        return;
      }
    case 0:
      echo $l;
      return;

    case 2:
      return $l;
  }
}

function logTrace($msg)
{
  global $serverConfig;
  if ($serverConfig->isDevPhase)
    echo $msg;
  else
    jmysql_query("insert into tracing set date=now(), content='" . addslashes($msg) . "'");
}

//-------------------------------------------------------------------------------------------------    
function creDate($dt, $offset)
{
  $d = clone($dt);
  $d->modify(($offset > 0 ? "+$offset" : $offset) . " day");
  return $d;
  //$t=mktime($dt->format("H"), $dt->format("i"), $dt->format("s"), $dt->format("n"), $dt->format("j"), $dt->format("Y"))+$offset*24*3600;
  //return new DateTime(date("Y-m-d H:m:s",$t));
}

$months = array("Année", "janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre");
$sMonths = array("An", "jan", "fév", "mar", "avr", "mai", "juin", "juil", "août", "sep", "oct", "nov", "déc");
$uMonths = array("ANNEE", "JANVIER", "FEVRIER", "MARS", "AVRIL", "MAI", "JUIN", "JUILLET", "AOUT", "SEPTEMBRE", "OCTOBRE", "NOVEMBRE", "DECEMBRE");
$orderMonths = array(1 => 5, 6, 7, 8, 9, 10, 11, 0, 1, 2, 3, 4);
$weekDays = array("dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi");
$sWeekDays = array("Di.", "Lu.", "Ma.", "Me.", "Je.", "Ve.", "Sa.");

//-------------------------------------------------------------------------------------------------    
function formatDate($dt, $ft = "W D M Y")
{
  global $weekDays, $sWeekDays, $months, $sMonths;
  for ($i = 0; $i < strlen($ft); $i++)
    switch ($ft[$i]) {
      case 'l':
        $s .= $weekDays[$dt->format("w")];
        break;

      case 'D':
        $s .= $sWeekDays[$dt->format("w")];
        break;

      case 'd':
        $s .= $dt->format("d");
        break;

      case 'F':
        $s .= $months[$dt->format("n")];
        break;

      case 'M':
        $s .= $sMonths[$dt->format("n")];
        break;

      case 'm':
        $s .= $dt->format("m");
        break;

      case 'n':
        $s .= $dt->format("n");
        break;

      case 'Y':
        $s .= $dt->format("Y");
        break;

      case 'y':
        $s .= $dt->format("y");
        break;

      case 'H':
        $s .= $dt->format("H");
        break;

      case 'i':
        $s .= $dt->format("i");
        break;

      case 's':
        $s .= $dt->format("s");
        break;

      default:
        $s .= $ft[$i];
    }
  return $s;
}

//-------------------------------------------------------------------------------------------------    
function getID($name, $avatar, $addr)
{
  $all = $name . $avatar . $addr;
  $len = strlen($all);
  if ($len < 8)
    $all .= "dsqf rtey ert yert y";
  $len = strlen($all);
  $i = 0;
  $j = 8;
  $bFirst = true;
  while ($i < 8) {
    for (; $j < $len; $j++) {
      $encoded[$i] = ord($all[$i]) ^ ord($all[$j]);
      $i++;
      if ($i == 8) {
        if ($bFirst)
          $i = 0;
        else
          break;
      }
    }
    $bFirst = false;
    $j = 8;
  }
  $res = "        ";
  for ($i = 0; $i < 8; $i++) {
    $encoded[$i] &= 0x7f;
    if ($encoded[$i] <= ord('0'))
      $encoded[$i] += ord('0');
    if ($encoded[$i] > ord('9') && $encoded[$i] < ord('A'))
      $encoded[$i] -= 7;
    if ($encoded[$i] > ord('Z') && $encoded[$i] < ord('a'))
      $encoded[$i] -= 6;
    if ($encoded[$i] > ord('z'))
      $encoded[$i] -= 5;
    $res[$i] = chr($encoded[$i]);
  }
  $all = substr($name, 0, 3) . '_' . strtolower(substr($avatar, 0, 3));
  for ($i = 0; $i < 7; $i++)
    switch ($all[$i]) {
      case 'ê':
      case 'ë':
      case 'é':
      case 'è':$all[$i] = 'e';
        break;
      case 'ï':$all[$i] = 'i';
        break;
      case 'ô':$all[$i] = 'o';
        break;
      case ' ':
      case "'":$all[$i] = '_';
        break;
      case 'ç':$all[$i] = 'c';
        break;
    }
  return "$all.$res";
}

//-------------------------------------------------------------------------------------------------    
function includeTabs($template = 1, $width = 100)
{
  echo "<script src=https://" . MAIN_DOMAIN_NAME . "/tools/tabcontent/tabcontent.js type=text/javascript></script>" . nl;
  echo "<link href=https://" . MAIN_DOMAIN_NAME . "/tools/tabcontent/template$template/tabcontent.css rel=stylesheet type=text/css>" . nl;
  echo "<div style='width:$width%;margin: 0 auto;'>" . nl;
  echo "   <ul class=tabs data-persist=true>" . nl;
}

//-------------------------------------------------------------------------------------------------    
function insertEl(&$ar, $row)
{
  for ($i = sizeof($ar) - 1; $i > $row; $i--)
    $ar[$i + 1] = $ar[$i];
  $ar[$row + 1] = '';
  ksort($ar);
}

//-------------------------------------------------------------------------------------------------    
function remEl(&$ar, $row)
{
  for ($i = $row; $i < sizeof($ar); $i++)
    $ar[$i] = $ar[$i + 1];
  unset($ar[$i - 1]);
}

//-------------------------------------------------------------------------------------------------
function delTree($dir)
{
  $files = array_diff(scandir($dir), array('.', '..'));
  foreach ($files as $file) {
    (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
  }
  return rmdir($dir);
}
?>
