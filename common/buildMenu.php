<?php

define("NAV_BRAND", 0);
define("NAV_SOCIAL", 1);
define("NAV_LINK", 2);
define("NAV_SEARCH", 3);
define("NAV_SUBMENU", 4);
define("NAV_COLOR", 5);
define("NAV_BACKGROUND", 6);
define("NAV_TITLE", 7);
define("NAV_LINK_RIGHT", 8);


define("NAV_SOCIAL_FB", 0);

define("NAV_LINK_TITLE", 0);
define("NAV_LINK_ACTION", 1);
define("NAV_LINK_TARGET", 2);
define("NAV_LINK_ICON", 3);

function buildMenu($verticalMenu, $horizontalMenu)
{
  if ($verticalMenu) {
    // Sidebar navigation
    echo ""
    . "<div id='slide-out' class='side-nav sn-bg-4 fixed'>"
    . " <ul class='custom-scrollbar'>"
    // Logo */
    . "  <li>"
    . "   <div class='logo-wrapper waves-light'>"
    . "    <a href='#'><img src='" . $verticalMenu[NAV_BRAND] . "' class='img-fluid flex-center'></a>"
    . "   </div>"
    . "  </li>\n";
    // /.Logo
    // Social
    if ($verticalMenu[NAV_SOCIAL]) {
      echo ""
      . "  <li>"
      . "   <ul class='social'>";
      foreach ($verticalMenu[NAV_SOCIAL] as $v) {
        echo ""
        . "    <li>"
        . "     <a href='" . $v["link"] . "'" . ($v["target"] ? " target=" . $v["target"] : '');
        switch ($v[NAV_SOCIAL_FB]) {
          case "facebook":
            echo ""
            . "       class='btn-floating btn-sm btn-fb' type='button' role='button'><i class='fab fa-facebook-f text-white align-top'></i></a>"
            . "    </li>";
            break;
        }
      }
      echo ""
      . "   </ul>"
      . "  </li>";
    }
    // /Social
    if ($verticalMenu[NAV_SEARCH]) {
      // Search Form */
      echo ""
      . "  <li>"
      . "   <form class='search-form' role='search'>"
      . "    <div class='form-group md-form mt-0 pt-1 waves-light'>"
      . "     <input type='text' class='form-control' placeholder='Search'>"
      . "    </div>"
      . "   </form>"
      . "  </li>\n";
      // /.Search Form
    }
    // Side navigation links
    echo ""
    . "  <li>"
    . "   <ul class='collapsible collapsible-accordion'>";
    foreach ($verticalMenu[NAV_LINK] as $v) {
      if (!isset($v[NAV_LINK_TITLE]) || $v["hide"]) {
        continue;
      }
      if ($v[NAV_LINK_NAME]) {
        echo ""
        . "    <li>"
        . "     <a href='" . $v[NAV_LINK_ACTION] . "'" . ($v["target"] ? " target=" . $v[NAV_LINK_TARGET] : '') . " class='waves-effect'>" . $v[NAV_LINK_TITLE] . "</a>"
        . "    </li>";
      } elseif ($v["NAV_SUBMENU"]) {
        echo ""
        . "    <li>"
        . "     <a class='collapsible-header waves-effect arrow-r'>"
        . "      <i class='fas fa-chevron-right'></i> "
        . $v[NAV_LINK_TITLE] .
        "        <i class='fas fa-angle-down  rotate-icon'></i>"
        . "     </a>"
        . "     <div class='collapsible-body'>"
        . "      <ul class='list-unstyled'>";
        foreach ($v[NAV_SUBMENU] as $v1) {
          echo ""
          . "     <li>"
          . "      <a href='" . $v1[NAV_LINK_ACTION] . "'" . ($v1[NAV_LINK_TARGET] ? " target=" . $v1[NAV_LINK_TARGET] : '') . " class='waves-effect'>" . $v1[NAV_LINK_TITLE] . "</a>"
          . "     </li>";
        }
        echo ""
        . "      </ul>"
        . "     </div>"
        . "    </li>\n";
      }
    }
    echo ""
    . "   </ul>"
    . "  </li>"
    ///. Side navigation links
    . " </ul>"
    . " <div class='sidenav-bg mask-strong'></div>"
    . "</div>\n";
  }
  // /. Sidebar navigation

  if (!$horizontalMenu) {
    return;
  }
  // Navbar
  echo ""
  . "<nav class='navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar " . ($verticalMenu ? "double-nav " : '') . $horizontalMenu[NAV_COLOR] . "'" . ($horizontalMenu[NAV_BACKGROUND] ? " style=background:" . $horizontalMenu[NAV_BACKGROUND] . ">" : '')
  // SideNav slide-out button
  . " <div class='float-left'>"
  . "  <a href='#' data-activates='slide-out' class='button-collapse black-text'><i style=color:#fff class='fas fa-bars'></i></a>"
  . " </div>\n";
  // Breadcrumb
  if ($horizontalMenu[NAV_BRAND]) {
    echo ""
    . " <div class='d-none d-md-block mr-auto' style=margin-left:100px>"
    . "  <img id=logo-navbar class='scrolling-navbar img-fluid' src=" . $horizontalMenu[NAV_BRAND] . ">"
    . " </div>\n";
  }
  echo " <ul class='nav navbar-nav nav-flex-icons mr-auto'>";
  buildMenuLinks($horizontalMenu[NAV_LINK]);
  echo " </ul>";
  if ($horizontalMenu[NAV_TITLE]) {
    echo ""
    . " <h5 class=mx-auto>$title</h5>";
  }
  if ($horizontalMenu[NAV_LINK_RIGHT]) {
    echo " <ul class='nav navbar-nav nav-flex-icons ml-auto'>";
    buildMenuLinks($horizontalMenu[NAV_LINK_RIGHT]);
    echo " </ul>";
  }

  echo "</nav>\n";
  // Navbar
}

function buildMenuLinks($menu)
{
  foreach ($menu as $v) {
    echo "  <li class='nav-item'>";
    if ($v[NAV_LINK_TITLE]) {
      echo ""
      . "   <a href='" . $v[NAV_LINK_ACTION] . "'" . ($v[NAV_LINK_TARGET] ? " target=" . $v[NAV_LINK_TARGET] : '') . " class='nav-link " . $horizontalMenu[NAV_COLOR] . "'>"
      . "      " . ($v[NAV_LINK_ICON] ? "<i class='" . $v[NAV_LINK_ICON] . "'></i>" : '') . " <span class='clearfix d-none d-sm-inline-block'>" . $v[NAV_LINK_TITLE] . "</span></a>";
    } else if ($v[NAV_SUBMENU]) {
      echo ""
      . "  <li class='nav-item dropdown'>"
      . "   <a class='nav-link dropdown-toggle " . $horizontalMenu[NAV_COLOR] . "' id='navbarDropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>" . $v[NAV_TITLE] . "'</a>"
      . "   <div class='dropdown-menu dropdown-menu-right' aria-labelledby='navbarDropdownMenuLink'>";
      foreach ($v[NAV_SUBMENU] as $v1) {
        echo ""
        . "    <a class='dropdown-item' href='" . $v1[NAV_LINK_ACTION] . "'" . ($v1[NAV_LINK_TARGET] ? " target=" . $v1[NAV_LINK_TARGET] : '') . ">" . $v1[NAV_LINK_TITLE] . "</a>";
      }
      echo "   </div>";
    } else if ($v[NAV_SEARCH]) {
      buildSearch($v[NAV_SEARCH]);
    }
    echo "  </li>\n";
  }
}
