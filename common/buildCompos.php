<?php

include_once "buildEdCompos.php";

function buildDropDown($params)
{
  global $mdbCompos;
  $common = $mdbCompos->getCommonAttr($params);
  echo "<div " . addAttribute($common->divAttr, 'dropdown') . ">";
  echo "  <" . $common->button . addAttribute($common->attr, "btn dropdown-toggle") . " type='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>" . $common->content . "</a>";
  foreach ($params[ED_OPTIONS] as $k => $options) {
    if (!is_array($options) && $k == ED_ATTR) {
      echo "  <div " . addAttribute($options, 'dropdown-menu') . ">";
      continue;
    }
    $option = $mdbCompos->getCommonAttr($options);
    if ($option->divAttr) {
      echo "<div " . $option->divAttr . "></div>";
    } else {
      echo "    <a class='dropdown-item' " . $option->attr . ">" . $option->content . "</a>";
    }
  }
  echo "  </div>";
  echo "</div>";
}

define("MODAL_SIZE", 1);
define("MODAL_SIZE_SMALL", "modal-sm");
define("MODAL_SIZE_LG", "modal-lg");
define("MODAL_SIZE_FLUID", "modal-fluid");

define("MODAL_POS", 2); //default top-centered
define("MODAL_POS_TOP", "modal-full-height modal-top");
define("MODAL_POS_RIGHT", "modal-full-height modal-right");
define("MODAL_POS_BOTTOM", "modal-full-height modal-bottom");
define("MODAL_POS_LEFT", "modal-full-height modal-left");
define("MODAL_POS_TOPRIGHT", "modal-side modal-top-right");
define("MODAL_POS_TOPLEFT", "modal-side modal-top-right");
define("MODAL_POS_BOTTOMRIGHT", "modal-side modal-top-right");
define("MODAL_POS_BOTTOMLEFT", "modal-side modal-top-right");

define("MODAL_DIR", 3); // default: top
define("MODAL_DIR_TOP", "top");
define("MODAL_DIR_RIGHT", "right");
define("MODAL_DIR_BOTTOM", "bottom");
define("MODAL_DIR_LEFT", "left");

define("MODAL_SCROLLABLE", 4);
define("MODAL_CENTERED", 5);
define("MODAL_NO_FADE", 6);
define("MODAL_NO_BACKDROP", 7);

define("MODAL_ID", 8);
define("MODAL_HEADER", 9);
define("MODAL_BODY", 10);
define("MODAL_FOOTER", 11);
define("MODAL_HEADER_ATTR", 12);
define("MODAL_FOOTER_ATTR", 13);
define("MODAL_CASCADING", 14);

function buildModal($params)
{
  $modal = "class='modal'";
  $dialog = "class='modal-dialog'";
  if ($tmp = $params[MODAL_SIZE]) {
    $dialog = addAttribute($dialog, $tmp);
  }
  if ($tmp = $params[MODAL_POS]) {
    $dialog = addAttribute($dialog, $tmp);
  }
  if ($tmp = $params[MODAL_DIR]) {
    $modal = addAttribute($modal, $tmp);
  }
  if ($tmp = $params[MODAL_SCROLLABLE]) {
    $dialog = addAttribute($dialog, "modal-dialog-scrollable");
  }
  if ($tmp = $params[MODAL_CENTERED]) {
    $dialog = addAttribute($dialog, "modal-dialog-centered");
  }
  if (!($tmp = $params[MODAL_NO_FADE])) {
    $modal = addAttribute($modal, "fade");
  }
  if ($tmp = $params[MODAL_NO_BACKDROP]) {
    $modal = addAttribute($modal, "false", "data-backdrop");
  }
  if ($tmp = $params[MODAL_ID]) {
    $modal = addAttribute($modal, $tmp, "id");
  } else {
    $modal = addAttribute($modal, "defaultModal", "id");
  }
  if ($tmp = $params[MODAL_CASCADING]) {
    $dialog = addAttribute($dialog, "cascading-modal",);
  }

  if ($tmp = $params[MODAL_HEADER]) {
    $header = "<div " . addAttribute($params[MODAL_HEADER_ATTR], "modal-header") . ">$tmp<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
  }
  if ($tmp = $params[MODAL_FOOTER]) {
    $footer = "<div " . addAttribute($params[MODAL_FOOTER_ATTR], "modal-footer") . ">$tmp</div>";
  }
  echo "<div $modal tabindex='-1' role='dialog'";
  echo "  aria-hidden='true'>";
  echo "  <div $dialog role='document'>";
  echo "    <div class='modal-content'>";
  echo "      $header";
  echo "      <div class='modal-body'>";
  echo "        " . $params[MODAL_BODY];
  echo "      </div>";
  echo "      $footer";
  echo "    </div>";
  echo "  </div>";
  echo "</div>  ";
}

define("SEARCH_BORDERED", 1);
define("SEARCH_LOOP_BEFORE", 2);
define("SEARCH_LOOP_AFTER", 3);
define("SEARCH_LOOP_INTEGRATED", 4);
define("SEARCH_PLACEHOLDER", 5);
define("SEARCH_COLOR", 6);
define("SEARCH_ACTIVE_COLOR", 7);
define("SEARCH_DIV_ATTR", 8);
define("SEARCH_ATTR", 9);
define("SEARCH_LOOP_COLOR", 10);
define("SEARCH_LOOP_ID", 11);

function buildSearch($param)
{
  if ($param[SEARCH_LOOP_INTEGRATED]) {
    if ($param[SEARCH_LOOP_BEFORE]) {
      echo ""
      . "<div class='input-group md-form form-sm form-1 ml-5 my-0'>"
      . "  <div class='input-group-prepend'>"
      . "    <span class='input-group-text " . $param[SEARCH_COLOR] . " lighten-3'" . ($param[SEARCH_LOOP_ID] ? " id=" . $param[SEARCH_LOOP_ID] : '') . ">"
      . "      <i class='fas fa-search " . ($param[SEARCH_LOOP_COLOR] ? $param[SEARCH_LOOP_COLOR] : "text-grey") . "' aria-hidden='true'></i>"
      . "    </span>"
      . "  </div>"
      . "  <input class='form-control my-0 py-1 " . ($param[SEARCH_COLOR] ? $param[SEARCH_COLOR] . "-border" : "") . "' type='text' placeholder='" . $param[SEARCH_PLACEHOLDER] . "' aria-label='Search'>";
    } else {
      echo ""
      . "<div class='input-group form-sm ml-5 my-0'>"
      . "  <input class='form-control my-0 py-1 " . ($param[SEARCH_COLOR] ? $param[SEARCH_COLOR] . "-border" : "") . "' type='text' placeholder='" . $param[SEARCH_PLACEHOLDER] . "' aria-label='Search'>"
      . "  <div class='input-group-append'>"
      . "    <span class='input-group-text " . $param[SEARCH_COLOR] . " lighten-3'" . ($param[SEARCH_LOOP_ID] ? " id=" . $param[SEARCH_LOOP_ID] : '') . ">"
      . "      <i class='fas fa-search " . ($param[SEARCH_LOOP_COLOR] ? $param[SEARCH_LOOP_COLOR] : "text-grey") . "' aria-hidden='true'></i>"
      . "    </span>"
      . "  </div>";
    }

    echo "</div>";
    return;
  }
  if ($param[SEARCH_LOOP_BEFORE] || $param[SEARCH_LOOP_AFTER]) {
    if ($param[SEARCH_BORDERED]) {
      if ($color = $param[SEARCH_COLOR]) {
        $color = "active-$color-3";
      }
      if ($color = $param[SEARCH_ACTIVE_COLOR]) {
        $color .= " active-$color-4";
      }
      echo ""
      . "<form class='form-inline $color'>"
      . "  <i class='fas fa-search' aria-hidden='true'></i>"
      . "  <input class='form-control form-control-sm " . $param[SEARCH_DIV_ATTR] . "' type='text' placeholder='" . $param[SEARCH_PLACEHOLDER] . "'"
      . "    aria-label='Search'>"
      . "</form>";
      return;
    }
    if ($color = $param[SEARCH_COLOR]) {
      $color = "active-$tmp";
    }
    if ($color = $param[SEARCH_ACTIVE_COLOR]) {
      $color .= " active-$tmp-2";
    }
    echo ""
    . "<form class='form-inline d-flex justify-content-center md-form form-sm $color'>"
    . "  <i class='fas fa-search' aria-hidden='true'></i>"
    . "  <input class='form-control form-control-sm ml-3 w-75' type='text' placeholder='Search'"
    . "    aria-label='Search'>"
    . "</form> ";
  }
}

define("BUTTON_OUTLINE", 0);
define("BUTTON_FLOATING", 1);
define("BUTTON_FLAT", 2);
define("BUTTON_ROUNDED", 3);
define("BUTTON_SIZE_SM", 4);
define("BUTTON_SIZE_LG", 5);
define("BUTTON_ICON_LEFT", 6);
define("BUTTON_ICON_RIGHT", 7);
define("BUTTON_FLUID", 8);
define("BUTTON_BLOCK", 8);
define("BUTTON_COLOR", 9);
define("BUTTON_TEXT", 10);
define("BUTTON_ATTR", 11);

function getButton($param)
{
  $attr = $param[BUTTON_ATTR];
  $color = $param[BUTTON_COLOR];
  $iconl = $param[BUTTON_ICON_LEFT];
  $iconr = $param[BUTTON_ICON_RIGHT];
  if ($param[BUTTON_FLOATING]) {
    if (!$iconl && !$iconr) {
      debugLog("l'icône est obligatoire pour le floating", null, true);
      exit();
    }
    $attr = addAttribute($attr, "btn-floating");
  } else {
    if ($param[BUTTON_OUTLINE]) {
      $attr = addAttribute($attr, "btn-outline-$color");
      unset($color);
    }
    if ($param[BUTTON_FLAT]) {
      $attr = addAttribute($attr, "btn-flat");
    }
    if ($param[BUTTON_ROUNDED]) {
      $attr = addAttribute($attr, "btn-rounded");
    }
    if ($param[BUTTON_FLUID]) {
      $attr = addAttribute($attr, "btn-block");
    }
    if ($param[BUTTON_SIZE_SM]) {
      $attr = addAttribute($attr, "btn-sm");
    } else if ($param[BUTTON_SIZE_LG]) {
      $attr = addAttribute($attr, "btn-lg");
    }
  }
  if ($color) {
    $attr = addAttribute($attr, "$color");
  }
  $out = extractAttr($attr, "href");
  $tag = $out[5][0] ? "a" : "button";

  return "<$tag " . addAttribute($attr, "btn") . ">$iconl" . $param[BUTTON_TEXT] . "$iconr</$tag>";
}

function getCloseIcon()
{
  return '<button type="button" class="close" aria-label="Close"><span aria-hidden="true">×</span></button>';
}