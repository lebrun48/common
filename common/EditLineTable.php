<?php

class EditLineTable
{

  public $row;
  public $formId = "editFormId";
  public $colDefinition;

  public function __construct($row = null)
  {
    $this->row = $row;
  }

  function buildForm()
  {
    echo " <form id=" . $this->formId . " class='needs-validation' novalidate>";
    $this->buildFormLine($this->colDefinition, $this->row);
    echo " </form>";
  }

  function buildFormLine($lines, $row)
  {
    foreach ($lines as $key => $params) {
      if ((($nextLine = $params[ED_NEXT_LINE]) || !$endDiv) && $params[ED_TYPE] != ED_TYPE_HIDDEN ) {
        echo "$endDiv<div class=form-row>";
        $endDiv = "</div>";
      }
      ($colWidth = $params[ED_FIELD_WIDTH]) || ($colWidth = 12);
      if (!$nextLine && ($totLine += $colWidth) > 12) {
        echo "$endDiv<div class=form-row>";
        $totLine = $colWidth;
      }
      $this->buildComponent($row, $key, $params);
    }
    echo $endDiv;
  }

  function buildComponent($row, $key, $param)
  {
    global $mdbCompos;
    if (isset($row[$key])) {
      $param[ED_VALUE] = $row[$key];
    }
    if (isset($param[ED_VALUE])) {
      unset($param[ED_PLACEHOLDER]);
    }
    if (!$param[ED_NAME]) {
      $param[ED_NAME] = $key;
    }
    $mdbCompos->buildCompos($param);
  }

  function getValue($row, $key)
  {
    return htmlspecialchars($row[$key], ENT_QUOTES);
  }

}
