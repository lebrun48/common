<?php

function getPrimary()
{
  global $request;
  if (!$request["primary"]) {
    return;
  }
  $table = $request["table"];
  global ${$table . "Cols"};
  $keysIdx = array_keys(${$table . "Cols"});
  foreach ($request["primary"] as $v) {
    $sql .= $keysIdx[$v[0]] . "='$v[1]' and";
  }
  return " where " . substr($sql, 0, -4);
}

if ($xAction) {
  if (!($file = $request["page"])) {
    $file = $request["table"];
  }
  switch ($xAction) {
    case "login":
      if (file_exists("axLogin.php")) {
        include $relativeCommonPath . "loginAllAjax.php";
        exit();
      }
      include "login.php";
      exit();

    case "edit":
      include "main.php";
      include_once $file . "Edit.php";
      switch ($action) {
        case "insert":
          break;

        case "update":
        case "display":
          $row = jmysql_fetch_assoc(jmysql_query("select * from $file " . getPrimary()));
          break;

        case "delete":
          break;
      }
      ($file . "ModalEdit")($row);
      exit();

    case "load":
      loadFile($file);
      if ($relativeCommonPath) {
        chdir($relativeCommonPath);
        loadFile($file);
      }
      exit();

    default:
      include "main.php";
      if ($file) {
        include_once $file . "Ajax.php";
        exit();
      }
  }
}

function loadFile($file)
{
  if (file_exists("$file.php")) {
    include "$file.php";
    buildMain();
    exit();
  }
  if (is_dir($file)) {
    chdir($file);
    include "main.php";
    buildMain();
    exit();
  }
}
