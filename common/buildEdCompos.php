<?php

include "composDefine.php";

class CommonAttr
{

  public $attr;
  public $content;
  public $label;
  public $id;
  public $validate;
  public $divAttr;

}

class MDBCompos
{

  public function buildCompos($param)
  {
    $params[ED_FIELD_WIDTH] || ($params[ED_FIELD_WIDTH] = 12); 
    switch ($param[ED_TYPE]) {
      case ED_TYPE_DATE:
        $param[ED_TEXT_BEFORE] = '<i class="fas fa-calendar" tabindex=0></i>';
        $param[ED_DIV_ATTR] = addAttribute($param[ED_DIV_ATTR], "datepicker");
        $name = $param[ED_NAME];
        $value = $param[ED_VALUE];
        $param[ED_DIV_ATTR] = addAttribute($param[ED_DIV_ATTR], json_encode($param[ED_DATE_PICKER]), "data-param");
        if ($value) {
          $param[ED_DIV_ATTR] = addAttribute($param[ED_DIV_ATTR], $value, "data-value");
        }
        unset($param[ED_VALUE]);
        $this->buildInput($param, "text");
        break;

      case ED_TYPE_FLOAT:
        if (!$param[ED_STEP_NUMBER]) {
          $param[ED_STEP_NUMBER] = "0.01";
        }
      case ED_TYPE_INTEGER:
        $type = "number";
      case ED_TYPE_ALPHA:
        if (!$type) {
          $type = "text";
        }

        $this->buildInput($param, $type);
        break;

      case ED_TYPE_TEXT:
        $this->buildText($param);
        break;

      case ED_TYPE_SELECT:
        $this->buildSelect($param);
        break;

      case ED_TYPE_HIDDEN:
        $this->buildHidden($param);
        break;

      case ED_TYPE_CHECK:
        $this->buildCheckBox($param);
        break;

      case ED_TYPE_RADIO:
        $this->buildRadio($param);
        break;
    }
  }

  public function getCommonAttr($params, $labelClass = null)
  {
    static $names = array();
    if (!is_array($params)) {
      $ret = new CommonAttr;
      $ret->content = $params;
      $ret->attr = '';
      return $ret;
    }
    $ret = new CommonAttr;
    $ret->attr = $params[ED_ATTR];
    $ret->divAttr = $params[ED_DIV_ATTR];
    if ($key = $params[ED_NAME]) {
      $ret->attr = addAttribute($ret->attr, "val_$key", "name");
    }
    if ($params[ED_SLIDE_UP]) {
      $ret->divAttr = addAttribute(addAttribute($ret->divAttr, "slide_$key", "id"), "display:none", "style");
    } else if ($params[ED_SLIDE_DOWN]) {
      $ret->divAttr = addAttribute($ret->divAttr, "slide_$key", "id");
    }

    if ($key) {
      $names[$key] ++;
      $ret->id = "ed_$key" . ($names[$key] == 1 ? '' : $names[$key] - 1);
      $ret->attr = addAttribute($ret->attr, $ret->id, "id");
    }
    if (($tmp = $params[ED_FIELD_WIDTH])) {
      $ret->divAttr = addAttribute($ret->divAttr, "col-$tmp", "class");
    }
    if (isset($params[ED_VALUE])) {
      $ret->attr = addAttribute($ret->attr, $params[ED_VALUE], "value");
    }
    if ($tmp = $params[ED_DISABLED]) {
      $ret->attr = addAttribute($ret->attr, '', "disabled");
    }
    if ($tmp = $params[ED_CHECKED]) {
      $ret->attr = addAttribute($ret->attr, '', "checked");
    }
    if ($tmp = $params[ED_SELECTED]) {
      $ret->attr = addAttribute($ret->attr, '', "selected");
    }
    if ($tmp = $params[ED_STEP_NUMBER]) {
      $ret->attr = addAttribute($ret->attr, $tmp, "step");
    }
    if ($tmp = $params[ED_PLACEHOLDER]) {
      $ret->attr = addAttribute($ret->attr, htmlentities($tmp, ENT_QUOTES), ($params[ED_TYPE] == ED_TYPE_SELECT ? "data-" : '') . "placeholder");
    }
    if (isset($params[ED_LABEL])) {
      $tmp = $params[ED_LABEL];
      $label = $this->getCommonAttr($tmp);
      $ret->label = "<label " . ($labelClass ? addAttribute($label->attr, $labelClass) : $label->attr) . ($ret->id ? " for=" . $ret->id : '') . ">" . $label->content . "</label>";
    }
    if ($tmp = $params[ED_VALIDATE]) {
      foreach ($tmp as $type => $val) {
        switch ($type) {
          case ED_VALIDATE_MAX_VALUE :
            $ret->attr = addAttribute($ret->attr, $val, "max");
            break;

          case ED_VALIDATE_MIN_VALUE :
            $ret->attr = addAttribute($ret->attr, $val, "min");
            break;

          case ED_VALIDATE_REQUIRED :
            $ret->attr = addAttribute($ret->attr, '', "required");
            break;

          case ED_VALIDATE_MAX_LENGTH :
            $ret->attr = addAttribute($ret->attr, $val, "maxlength");
            break;

          case ED_VALIDATE_MIN_LENGTH :
            $ret->attr = addAttribute($ret->attr, $val, "minlength");
            break;

          case ED_VALIDATE_VALIDE :
            $ret->validate .= '<div class="valid-feedback">' . $val . '</div>';
            break;

          case ED_VALIDATE_INVALIDE :
            $ret->validate .= '<div class="invalid-feedback">' . $val . '</div>';
            break;
        }
      }
    }
    if (($tmp = $params[ED_TEXT_BEFORE]) && ($isBefore = true) || ($tmp = $params[ED_TEXT_AFTER])) {
      if (substr($tmp, 0, 3) == '<i ') {
        $ret->prepend = str_replace("class=\"", "class=\"input-prefix ", $tmp);
        $ret->divAttr = addAttribute($ret->divAttr, "input-with-" . ($isBefore ? "pre" : "post") . "-icon");
      } else {
        $ret->divAttr = addAttribute($ret->divAttr, "input-group");
        if ($isBefore) {
          $ret->prepend = "<div class=input-group-prepend><span class='input-group-text md-addon'>$tmp</span></div>";
        } else {
          $ret->append = "<div class=input-group-append><span class='input-group-text md-addon'>$tmp</span></div>";
        }
      }
    }
    $ret->content = $params[ED_CONTENT];
    $ret->button = "a ";
    if ($tmp = $params[ED_TYPE_BUTTON]) {
      $ret->button = "button ";
      $ret->attr = addAttribute($ret->attr, "button", "type");
    }

    return $ret;
  }

  function buildSwitch($switch)
  {
    $common = $this->getCommonAttr($switch);
    echo "<div " . addAttribute($common->divAttr, "switch") . ">";
    echo " <label>" . $switch[ED_LABEL_OFF];
    echo "    <input type=checkbox " . $common->attr . ">";
    echo "    <span class='lever'></span>" . $switch[ED_LABEL_ON];
    echo "  </label>";
    echo "</div>";
  }

  function buildInput($input, $type)
  {
    $common = $this->getCommonAttr($input);
    echo "<div " . addAttribute($common->divAttr, "md-form form-sm") . ">";
    echo "  <input type=$type " . addAttribute($common->attr, "form-control") . ">";
    echo $common->prepend;
    echo $common->append;
    echo $common->label;
    echo $common->validate;
    echo "</div>";
  }

  function buildText($input)
  {
    echo "<div class=input-group-prepend><span class='input-group-text md-addon'>" . $input[ED_VALUE] . "</span></div>";
  }

  function buildCheckBox($check) //radio or checkbox
  {
    $common = $this->getCommonAttr($check, "form-check-label");
    echo "<div class=form-check>";
    echo "  <input type=checkbox " . addAttribute($common->attr, "form-check-input") . $common->checked . ">";
    echo $common->prepend;
    echo $common->append;
    echo $common->label;
    echo $common->validate;
    echo "</div>";
  }

  function buildRadio($radio)
  {
    $value = $radio[ED_VALUE];
    unset($radio[ED_VALUE]);
    ($width = $radio[ED_FIELD_WIDTH]) || ($width = 12);
    $radio[ED_FIELD_WIDTH] = $width / sizeof($radio[ED_OPTIONS]);
    $common = $this->getCommonAttr($radio);
    foreach ($radio[ED_OPTIONS] as $i => $options) {
      if (isset($value)) {
        $options[ED_CHECKED] = $value == $options[ED_VALUE];
      }
      $options[ED_NAME] = $radio[ED_NAME];
      $option = $this->getCommonAttr($options, "form-check-label");
      echo "<div " . addAttribute($common->divAttr, "md-form form-sm") . ">";
      echo "  <input type=radio " . addAttribute($option->attr, "form-check-input") . ">";
      echo $option->prepend;
      echo $option->append;
      echo $option->label;
      echo $option->validate;
      echo "</div>";
    }
  }

  function buildSelect($select)
  {
    $value = $select[ED_VALUE];
    unset($select[ED_VALUE]);
    $common = $this->getCommonAttr($select, "mdb-main-label");
    echo "<div " . $common->divAttr . ">";
    if ($select[ED_VALIDATE]) {
      $common->attr = addAttribute($common->attr, "true", "data-validate");
    }
    echo "<select " . $common->attr . " class='mdb-select md-form'>";
    foreach ($select[ED_OPTIONS] as $option) {
      if (isset($value) && (isset($option[ED_VALUE]) && $option[ED_VALUE] == $value || !isset($option[ED_VALUE]) && $value == $option[ED_CONTENT])) {
        $option[ED_SELECTED] = true;
      }
      $optionAttr = $this->getCommonAttr($option);
      echo "<option " . $optionAttr->attr . ">" . $optionAttr->content . "</option>";
    }
    echo "</select>";
    echo $common->label;
    echo $common->validate;
    echo "</div>";
  }

  function buildHidden($params)
  {
    echo "<input type=hidden name=val_" . $params[ED_NAME] . " id=ed_" . $params[ED_NAME] . " value='" . $params[ED_VALUE] . "'>";
  }

}

function addAttribute($attr, $value, $key = "class")
{
  if (!isset($value)) {
    if (($pos = strpos($attr, $key)) !== false && (($tmp = $attr[$pos + strlen($key)]) == '' || $tmp == ' ')) {
      return $attr;
    }
    return $attr . " $key";
  }

  $valAttr = preg_split("/[\s;]/", $value);
  $sep = $key == "style" ? ";" : " ";
  $out = extractAttr($attr, $key);
  if ($out[2][0]) {
    $valOut = preg_split("/[\s;]/", $out[5][0]);
    foreach ($valAttr as $k => $v) {
      if (array_search($v, $valOut) !== false) {
        unset($valAttr[$k]);
      }
    }
    if (!sizeof($valAttr)) {
      return $attr;
    }
    $valAttr = array_merge($valAttr, $valOut);
    $value = implode($sep, $valAttr);
    return substr($attr, 0, $out[1][1]) . "$key='$value'" . substr($attr, $out[3][1] + strlen($out[3][0]));
  }
  return "$attr $key='$value'";
}

function extractAttr($attr, $key)
{
  preg_match("/($key)\s*(=)\s*((\"|')(.*)[\"'])?/", $attr, $out, PREG_OFFSET_CAPTURE);
  if ($out[2][0]) {
    if (!$out[4][0]) {
      $out[5][0] = strtok(substr($attr, $out[2][1] + 1), " ");
      $out[3][0] = $out[5][0];
      $out[3][1] = $out[2][1] + 1;
    }
  }
  return $out;
}

function arrayInsertAfter(array &$array, $key, array $new)
{
  $keys = array_keys($array);
  $index = array_search($key, $keys);
  $pos = false === $index ? count($array) : $index + 1;

  $array = array_merge(array_slice($array, 0, $pos), $new, array_slice($array, $pos));
}

$mdbCompos = new MDBCompos();
