<?php
define("ED_SECTIONS_COMPOS", 0);
define("ED_SECTIONS_LABEL", 1);

define("ED_TYPE_INTEGER", 0);
define("ED_TYPE_FLOAT", 1);
define("ED_TYPE_ALPHA", 2);
define("ED_TYPE_DATE", 3);
define("ED_TYPE_CHECK", 4);
define("ED_TYPE_SWITCH", 5);
define("ED_TYPE_SELECT", 6);
define("ED_TYPE_HIDDEN", 7);
define("ED_TYPE_TEXT", 8);
define("ED_TYPE_BUTTON", 9); //default link (<a ..>)
define("ED_TYPE_RADIO", 10);

define("ED_VALIDATE_MAX_LENGTH", 0);
define("ED_VALIDATE_MIN_LENGTH", 1);
define("ED_VALIDATE_REQUIRED", 2);
define("ED_VALIDATE_MAX_VALUE", 3);
define("ED_VALIDATE_MIN_VALUE", 4);
define("ED_VALIDATE_FUNCTION", 5);
define("ED_VALIDATE_INVALIDE", 6);
define("ED_VALIDATE_VALIDE", 7);


define("ED_TEXT_AFTER", 100);
define("ED_TEXT_BEFORE", 101);
define("ED_LABEL", 102);
define("ED_ATTR", 103);
define("ED_TYPE", 104);
define("ED_CHECKED", 105);
define("ED_SIZE", 106);
define("ED_VALUE", 107);
define("ED_NAME", 108);
define("ED_DISABLED", 109);
define("ED_SELECTED", 110);
define("ED_OPTIONS", 111);
define("ED_LABEL_ON", 112);
define("ED_LABEL_OFF", 113);
define("ED_CONTENT", 114);
define("ED_VALIDATE", 115);
define("ED_NEXT_LINE", 116);
define("ED_SLIDE_UP", 117);
define("ED_SLIDE_DOWN", 118);
define("ED_STEP_NUMBER", 119);
define("ED_PLACEHOLDER", 120);
define("ED_DIV_ATTR", 121);
define("ED_DATE_PICKER", 122);
define("ED_FIELD_WIDTH", 123);

